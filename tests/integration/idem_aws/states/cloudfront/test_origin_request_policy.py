import copy
import uuid
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-origin_request_policy-" + str(uuid.uuid4()),
    "comment": "creating cloudfront origin request policy for idem test",
}
STATE_NAME = "aws.cloudfront.origin_request_policy"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    # skipping test in localstack as cloudfront origin_request_policy creation is not supported in localstack.
    global PARAMETER, STATE_NAME
    ctx["test"] = __test

    PARAMETER["headers_config"] = {
        "HeaderBehavior": "whitelist",
        "Headers": {
            "Quantity": 3,
            "Items": [
                "origin",
                "access-control-request-headers",
                "access-control-request-method",
            ],
        },
    }
    PARAMETER["cookies_config"] = {"CookieBehavior": "none"}
    PARAMETER["query_strings_config"] = {"QueryStringBehavior": "none"}

    ret = await hub.states.aws.cloudfront.origin_request_policy.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )

    assert not ret["old_state"] and ret["new_state"]
    assert_origin_request_policy(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    # skipping test in localstack as cloudfront origin_request_policy creation is not supported in localstack.

    global PARAMETER, STATE_NAME
    describe_ret = await hub.states.aws.cloudfront.origin_request_policy.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that describe output format is correct
    assert STATE_NAME + ".present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(STATE_NAME + ".present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert_origin_request_policy(described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update_origin_request_policy", depends=["describe"])
async def test_update(hub, ctx, __test):
    # skipping test in localstack as cloudfront origin_request_policy creation is not supported in localstack.

    global PARAMETER, STATE_NAME
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    new_parameter["headers_config"] = {
        "HeaderBehavior": "whitelist",
        "Headers": {
            "Quantity": 2,
            "Items": ["origin", "access-control-request-headers"],
        },
    }
    ret = await hub.states.aws.cloudfront.origin_request_policy.present(
        ctx, **new_parameter
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )

    old_resource = ret["old_state"]
    assert_origin_request_policy(old_resource, PARAMETER)

    resource = ret["new_state"]
    assert_origin_request_policy(resource, new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["update_origin_request_policy"])
async def test_absent(hub, ctx, __test):
    # skipping test in localstack as cloudfront origin_request_policy creation is not supported in localstack.

    ctx["test"] = __test
    ret = await hub.states.aws.cloudfront.origin_request_policy.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert_origin_request_policy(old_resource, PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    # skipping test in localstack as cloudfront origin_request_policy creation is not supported in localstack.

    ctx["test"] = __test
    ret = await hub.states.aws.cloudfront.origin_request_policy.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_origin_request_policy_absent_with_none_resource_id(hub, ctx):
    origin_request_policy_temp_name = "idem-test-origin_request_policy-" + str(
        uuid.uuid4()
    )
    # Delete origin_request_policy with resource_id as None. Result in no-op.
    ret = await hub.states.aws.cloudfront.origin_request_policy.absent(
        ctx, name=origin_request_policy_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=origin_request_policy_temp_name
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.cloudfront.origin_request_policy.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def assert_origin_request_policy(resource, parameters):
    assert parameters.get(
        "parameter_in_cache_key_and_forward_to_origin"
    ) == resource.get("parameter_in_cache_key_and_forward_to_origin")
    assert parameters.get("comment") == resource.get("comment")
    assert parameters.get("headers_config") == resource.get("headers_config")
    assert parameters.get("cookies_config") == resource.get("cookies_config")
    assert parameters.get("query_strings_config") == resource.get(
        "query_strings_config"
    )
