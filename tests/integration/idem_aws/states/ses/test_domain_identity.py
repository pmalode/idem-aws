import uuid
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-domain_id-" + str(uuid.uuid4()),
    "domain": "example.com",
}


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.ses.domain_identity.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    if __test:
        assert (
            f"Would create aws.ses.domain_identity '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            f"Created aws.ses.domain_identity '{PARAMETER['name']}'" in ret["comment"]
        )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert PARAMETER["domain"] == resource.get("domain")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    global PARAMETER
    describe_ret = await hub.states.aws.ses.domain_identity.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.ses.domain_identity.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.ses.domain_identity.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["domain"] == described_resource_map.get("domain")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["present"])
async def test_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    ret = await hub.states.aws.ses.domain_identity.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["domain"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    old_resource = ret["old_state"]
    assert PARAMETER["domain"] == old_resource.get("domain")
    if __test:
        assert (
            f"Would delete aws.ses.domain_identity '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Deleted aws.ses.domain_identity '{PARAMETER['name']}'" in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER
    ctx["test"] = __test
    # Delete same identity again.
    ret = await hub.states.aws.ses.domain_identity.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["domain"]
    )
    assert ret["result"], ret["comment"]
    assert (
        f"aws.ses.domain_identity '{PARAMETER['name']}' already absent"
        in ret["comment"]
    )
    assert not ret.get("old_state") and not ret.get("new_state")
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_domain_id_absent_with_none_resource_id(hub, ctx):
    domain_id_temp_name = "idem-test-domain_id-" + str(uuid.uuid4())
    # Delete domain identity with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ses.domain_identity.absent(
        ctx, name=domain_id_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.ses.domain_identity '{domain_id_temp_name}' already absent"
        in ret["comment"]
    )
