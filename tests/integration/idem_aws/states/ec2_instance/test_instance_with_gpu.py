"""
Test creation and deletion of an instance with GPUs
"""
import pytest


# Parametrization options for running each test with --test first and then without --test
@pytest.mark.localstack(
    False, "LocalStack/Pro does not support Elastic Graphics accelerators"
)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name, aws_ec2_windows_ami, __test):
    gpu_type = "eg1.medium"
    """
    Create an instance with Elastic Graphics accelerator
    """
    ret = await hub.states.aws.ec2.instance.present(
        ctx,
        name=instance_name,
        # use a relatively small type which supports Elastic Graphics accelerators
        instance_type="t2.medium",
        # choose a windows image (Elastic Graphics is for windows instances) which works with the instance type
        image_id=aws_ec2_windows_ami["resource_id"],
        client_token=instance_name,
        tags={"Name": instance_name},
        elastic_gpu_specifications=[{"Type": gpu_type}],
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]

    if ctx.test:
        return

    # Wait for the resource to exist and be running
    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "Instance", ret["new_state"]["resource_id"]
    )
    await hub.tool.boto3.resource.exec(resource, "wait_until_exists")
    await hub.tool.boto3.resource.exec(resource, "wait_until_running")

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.ec2.instance.get(
        ctx, resource_id=ret["new_state"]["resource_id"]
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == ret["new_state"]["resource_id"]
    assert get.ret["elastic_gpu_specifications"]

    egpu_id = get.ret["elastic_gpu_specifications"][0]["ElasticGpuId"]

    # Verfiy the Elastic GPU Type
    get_egpu = await hub.exec.boto3.client.ec2.describe_elastic_gpus(
        ctx, ElasticGpuIds=[egpu_id]
    )
    assert get_egpu.result, get_egpu.comment
    assert get_egpu.ret, get_egpu.comment
    assert get_egpu.ret["ElasticGpuSet"][0]["ElasticGpuType"] == gpu_type


@pytest.mark.localstack(
    False, "LocalStack/Pro does not support Elastic Graphics accelerators"
)
@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name, __test):
    """
    Destroy the instance created by the present state
    """
    get = await hub.exec.aws.ec2.instance.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [instance_name]}]
    )

    ret = await hub.states.aws.ec2.instance.absent(
        ctx, name=instance_name, resource_id=get.ret.resource_id
    )
    assert ret["result"], ret["comment"]

    if not ctx.test:
        # Wait until terminated so the subnet doesn't have any leftover dependencies
        resource = await hub.tool.boto3.resource.create(
            ctx, "ec2", "Instance", get.ret.resource_id
        )
        await hub.tool.boto3.resource.exec(resource, "wait_until_terminated")
