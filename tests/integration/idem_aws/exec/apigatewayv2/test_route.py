import time

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get(hub, ctx, aws_apigatewayv2_route):
    ret = await hub.exec.aws.apigatewayv2.route.get(
        ctx,
        name=aws_apigatewayv2_route["name"],
        api_id=aws_apigatewayv2_route["api_id"],
        route_id=aws_apigatewayv2_route["route_id"],
    )
    assert "result" in ret
    assert "comment" in ret
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_apigatewayv2_route["route_key"] == resource.get("route_key")
    assert aws_apigatewayv2_route["route_id"] == resource.get("route_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get_invalid_route_id(hub, ctx, aws_apigatewayv2_route):
    api_get_name = "idem-test-exec-get-api-" + str(int(time.time()))
    ret = await hub.exec.aws.apigatewayv2.route.get(
        ctx,
        name=api_get_name,
        api_id=aws_apigatewayv2_route["api_id"],
        route_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.apigatewayv2.route", name=api_get_name
    ) in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_list(hub, ctx, aws_apigatewayv2_route):
    ret = await hub.exec.aws.apigatewayv2.route.list(
        ctx,
        api_id=aws_apigatewayv2_route["api_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = {}
    for entry in ret["ret"]:
        if entry["resource_id"] == aws_apigatewayv2_route["resource_id"]:
            resource = entry
            break
    assert aws_apigatewayv2_route["route_key"] == resource.get("route_key")
    assert aws_apigatewayv2_route["route_id"] == resource.get("route_id")
