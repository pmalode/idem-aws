import time

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get(hub, ctx, aws_certificate_manager_fqdns):
    certificate_get_name = "idem-test-exec-get-certificate-" + str(int(time.time()))
    ret = await hub.exec.aws.acm.certificate_manager.get(
        ctx,
        name=certificate_get_name,
        resource_id=aws_certificate_manager_fqdns["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_certificate_manager_fqdns["resource_id"] == resource.get("resource_id")
    assert certificate_get_name == resource.get("name")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get_invalid_resource_id(hub, ctx):
    certificate_get_name = "idem-test-exec-get-certificate-" + str(int(time.time()))
    ret = await hub.exec.aws.acm.certificate_manager.get(
        ctx,
        name=certificate_get_name,
        resource_id="invalid-arn",
    )
    assert not ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"ParamValidationError: Parameter validation failed:\\n"
        f"Invalid length for parameter CertificateArn, value: 11, valid min length: 20"
        in str(ret["comment"])
    )
