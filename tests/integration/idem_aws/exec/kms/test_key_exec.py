import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_kms_key):
    kms_get_name = "idem-test-exec-get-kms-" + str(int(time.time()))
    ret = await hub.exec.aws.kms.key.get(
        ctx,
        name=kms_get_name,
        resource_id=aws_kms_key["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_kms_key["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    kms_get_name = "idem-test-exec-get-kms-" + str(int(time.time()))
    ret = await hub.exec.aws.kms.key.get(
        ctx,
        name=kms_get_name,
        resource_id="@Fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.kms.key '{kms_get_name}' result is empty" in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_kms_key):
    kms_get_name = "idem-test-exec-list-kms-" + str(int(time.time()))
    ret = await hub.exec.aws.kms.key.list(ctx, name=kms_get_name)
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    resource_id_list = []
    for key in ret["ret"]:
        resource_id_list.append(key["resource_id"])
    assert aws_kms_key["resource_id"] in resource_id_list
