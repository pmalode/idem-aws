from unittest import mock

import pytest

RAW_PROFILE = {
    "aws_access_key_id": "my_key_id",
    "aws_secret_access_key": "my_secret_key",
    "region": "us-east-1",
    "assume_role": {
        "role_arn": "arn:aws:iam::999999999:role/xacct/administrator",
        "external_id": "test",
        "role_session_name": "Idem",
    },
    "esm": {"unmodified_key": "unmodified_value"},
}


@pytest.mark.asyncio
async def test_gather(hub, mock_hub):
    hub.tool.boto3.client.exec = mock_hub.tool.boto3.client.exec
    hub.tool.boto3.client.exec.return_value = {
        "Credentials": {
            "AccessKeyId": "my_new_key_id",
            "SecretAccessKey": "my_new_key",
            "SessionToken": "my_new_token",
        }
    }

    with mock.patch("botocore.config.Config") as config:
        p1 = {}
        await hub.tool.aws.acct.assume_role.modify(
            name="name",
            raw_profile=RAW_PROFILE,
            new_profile=p1,
        )

        mock_hub.tool.boto3.client.exec.assert_called_once_with(
            {
                "acct": {},
                "test": False,
            },
            "sts",
            "assume_role",
            RoleArn="arn:aws:iam::999999999:role/xacct/administrator",
            RoleSessionName="Idem",
            ExternalId="test",
        )

    assert p1["aws_access_key_id"] == "my_new_key_id"
    assert p1["aws_secret_access_key"] == "my_new_key"
    assert p1["aws_session_token"] == "my_new_token"
